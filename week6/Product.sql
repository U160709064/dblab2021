-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema Product
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `Product` ;

-- -----------------------------------------------------
-- Schema Product
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Product` DEFAULT CHARACTER SET utf8 ;
USE `Product` ;

-- -----------------------------------------------------
-- Table `CATEGORY`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `CATEGORY` ;

CREATE TABLE IF NOT EXISTS `CATEGORY` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `descrioption` TEXT(55) NOT NULL,
  `CATEGORYcol2` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Supplier`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Supplier` ;

CREATE TABLE IF NOT EXISTS `Supplier` (
  `id` INT NOT NULL,
  `compamyname` VARCHAR(255) NOT NULL,
  `contactname` VARCHAR(255) NOT NULL,
  `address` TEXT(55) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `postalcode` INT NOT NULL,
  `country` VARCHAR(45) NOT NULL,
  `phone` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PRODUCT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PRODUCT` ;

CREATE TABLE IF NOT EXISTS `PRODUCT` (
  `id` INT NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `CATEGORY_id` INT NOT NULL,
  `Supplier_id` INT NOT NULL,
  `unit` VARCHAR(255) NOT NULL,
  `price` FLOAT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_PRODUCT_CATEGORY`
    FOREIGN KEY (`CATEGORY_id`)
    REFERENCES `CATEGORY` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PRODUCT_Supplier1`
    FOREIGN KEY (`Supplier_id`)
    REFERENCES `Supplier` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_PRODUCT_CATEGORY_idx` ON `PRODUCT` (`CATEGORY_id` ASC) VISIBLE;

CREATE INDEX `fk_PRODUCT_Supplier1_idx` ON `PRODUCT` (`Supplier_id` ASC) VISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
